
const apiKey = "fe241c68";
const searchBt = document.getElementById("search");
const image = document.getElementById("movieImg");
const title = document.getElementById("title");
const description = document.getElementById("description");
const inputTitle = document.getElementById("inputTitle");
let theMovie = "";

async function getMovie(title) {

    image.src = "";
    title.innerHTML = "";
    description.src = "";
    image.hidden = false;
    description.hidden = false;

    let url = `https://omdbapi.com/?apikey=${apiKey}&t=${title}`;
    let url2 = `https://omdbapi.com/?apikey=${apiKey}&i=${title}`;
    let  response = await fetch(url);
    let  data = await response.json();
    if (data.Response == "False") {
        response = await fetch(url2);
        data = await response.json();
        if (data.Response == "False") {
            throw Error = "Pelicula no encontrada";
        }
    }

    inputTitle.value = "";
    return data;
};

searchBt.addEventListener("click", () => {
    let titlemov = inputTitle.value;
    let movie = getMovie(titlemov)
        .then(response => {
            image.src = response.Poster;
            title.innerHTML = response.Title;
            description.innerHTML = response.Plot;
        })
        .catch(error => {
            image.hidden = true;
            title.innerHTML = error;
            description.hidden = true;
        });
});

